## This directory contains datasets supporting the *solance* project.

### The content of each file is listed as follows:

- `lola.csv`: the original 1535 spatial locations, on which field measurements, NAM outputs, and SREF outputs are observed, defined by latitude and longitude

- `nam_new.csv`: the new 1000 NAM outputs, 22 of which are missing, obtained from IBM PAIRS

- `sol_ta.csv`: the time-aggregated dataset, including the field measurements, NAM outputs, and SREF outputs

- `daily.csv`: the daily dataset, including the field measurements, NAM outputs, and SREF outputs

- `lola_new.csv`: the spatial locations on which the new 1000 NAM outputs are obtained (978 locations in total)

- `nam_new10k.csv`: the new 10000 NAM outputs, 302 of which are missing, obtained from IBM PAIRS