## This directory contains data and examples from *solar irradiance* project, formerly residing in the `laGP` mercurial repository

Maintainers: Robert B. Gramacy (Virginia Tech) and Furong Sun (Virginia Tech)

- This work is in collaboration with Ben Haaland (University of Utah), Siyuan Lu (IBM Thomas J. Watson Research Center) and Youngdeok Hwang (Sungkyunkwan University).
- For more details, see our [technical report on the arXiv](https://arxiv.org/abs/1806.05131); accepted by *Statistical Analysis and Data Mining*.

### A brief description of the contents of each sub-directory follows.

- `R`: the code, particularly the one generating all the figures in the paper linked above; See README.md therein for details

- `data`: the datasets referenced by the code in `R'; See README.md therein for details
